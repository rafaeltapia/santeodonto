import metrics from './metrics';
import colors from './colors';
import fonts from './fonts';

const general = {
  container: {
    flex: 1,
    backgroundColor: colors.background,
  },

  input: {
    fontFamily: 'Montserrat-Medium',
    borderRadius: 8,
    borderColor: colors.border,
    color: colors.font,
    fontSize: fonts.input,
    borderWidth: 1,
    height: 57,
    textAlign: 'center',
    marginBottom: 17
  },

  botao: {
    borderRadius: 8,
    height: 57,
    backgroundColor: colors.primary,
    alignItems: "center",
    justifyContent: 'center',
    marginBottom: 12
  },

  botaoTexto: {
    fontFamily: 'Montserrat-Bold',
    fontWeight: 'bold',
    color: colors.fontText,
    fontSize: fonts.input,
  },

  erroTexto: {
    fontFamily: 'Montserrat-Regular',
    fontSize: fonts.regular,    
    textAlign: 'left',
    color: colors.textError,
    paddingTop: 18    
  },

};

export default general;