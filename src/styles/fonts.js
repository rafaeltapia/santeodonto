const fonts = {
    input: 14,
    large: 23,
    regular: 13,
    medium: 12,
    small: 11,
    tiny: 10,
  };
  
  export default fonts;