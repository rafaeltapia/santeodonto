const colors = {    
    primary: '#09A2F1',
    background: '#FFF',
    font: '#646464',
    border: '#ECECEC',
    fontText: '#FFF',
    textError: '#FF0000'
  };
  
  export default colors;