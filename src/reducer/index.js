import { combineReducers } from 'redux';
import SplashScreenReducer from './SplashScreenReducer';
import LoginReducer from './LoginReducer';

export default combineReducers({
    SplashScreenReducer,
    LoginReducer
});