import React, { Component } from 'react';
import {
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  Text,
  Modal
} from 'react-native';

import { Icon } from 'native-base';

import styles from './styles';
import { logo, success, circleLeft, circleRight } from '../../static/images_require';

class Login extends Component {

  constructor() {
    super()

    this.state = {      
      email: '',
      modalVisible: false,
    }
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.logo}>
          <Icon style={{ flex: 0.2, fontSize: 40, alignSelf: 'flex-start', color: '#fff' }} type='MaterialIcons' name='chevron-left' />
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-around'}}>            
            <Image style={styles.logoImagem} source={logo} />
            <Text style={styles.titulo}>Esqueci minha <Text style={styles.tituloBold}>senha</Text></Text>
            <Text style={styles.subTitulo}>Insira seu e-mail cadastrado e clique em enviar</Text>
          </View>
        </View>

        <View style={styles.forms}>
          <TextInput style={styles.input}
            autoCapitalize={'none'}
            onChangeText={email => this.setState({ email })}
            placeholder={'E-mail'}
          />

          <TouchableOpacity onPress={() => {this.setModalVisible(true)}}
              style={styles.botao}>
            <Text style={styles.botaoTexto}>
              Enviar
            </Text>
          </TouchableOpacity>

          <Text style={styles.erroTexto}>*e-mail inválido</Text>
        
        </View>

        <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.modalVisible}>
          <View style={styles.modalContainer}>
            <View style={styles.modal}>
              <Image source={success} style={{marginTop: 38}}/>

              <Text style={styles.textoSucesso}>Sucesso!</Text>

              <Text style={styles.descricaoSucesso}>Siga os passos que recebeu no seu e-mail para redefinir sua senha.!</Text>

              <TouchableHighlight
                  style={styles.botaoConfirmar}
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}>
                  <Text style={styles.botaoConfirmarTexto}>OK</Text>
              </TouchableHighlight>              
            </View>
          </View>
        </Modal>

        <View style={styles.footer}>
            <Image style={styles.footerLeft} source={circleLeft}/>
            <Image style={styles.footerRight} source={circleRight}/>
          </View>

      </View>
    )
  }
}

export default Login;
