import { general, colors, fonts } from '../../styles';
import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  ...general,
 
  logo: {
    backgroundColor: colors.primary,      
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 60,
    flex: 0.3
  },

  logoImagem: {
      width: 136,
      height: 46
  },

  titulo: {
      fontFamily: 'Montserrat-Medium',
      fontSize: fonts.large,
      color: colors.fontText,
      paddingTop: 32,
      paddingLeft: 55,
      paddingRight: 55,
      paddingBottom: 13
  },

  tituloBold: {
      fontFamily: 'Montserrat-Medium',
      fontWeight: 'bold'
  },

  subTitulo: {
    fontFamily: 'Montserrat-Light',
    textAlign: 'center',
    fontSize: fonts.regular,
    color: colors.fontText,    
    paddingLeft: 88,
    paddingRight: 88,
    paddingBottom: 41
  },

  forms: {
    flex: 0.6,
    paddingLeft: 22,
    paddingRight: 22,
    paddingTop: 53,    
    alignItems: "stretch",
  },

  modalContainer: {
    backgroundColor: 'rgba(0,0,0,0.5)', 
    height: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  modal: {
    width: 281, 
    height: 288, 
    backgroundColor: 'white',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center'
  },

  textoSucesso: {
    paddingTop: 22,
    paddingBottom: 15,
    fontFamily: 'Montserrat-Bold',
    fontWeight: 'bold',
    fontSize: 17,
    textAlign: 'center'
  },

  descricaoSucesso: {
    fontFamily: 'Montserrat-Light',
    textAlign: 'center',
    fontSize: fonts.regular,
    paddingBottom: 15,
    paddingLeft: 21,
    paddingRight: 21
  },  

  botaoConfirmar: {    
    borderRadius: 8,
    width: '80%',
    marginLeft: 39,
    marginRight: 39,
    height: 38,
    backgroundColor: colors.primary,
    alignItems: "center",
    justifyContent: 'center',
    marginBottom: 37
  },

  botaoConfirmarTexto: {
    fontFamily: 'Montserrat-Bold',
    fontWeight: 'bold',
    color: colors.fontText,
    fontSize: fonts.input,
  },

  footer: {
    flex: 0.1,
    flexDirection: 'row',
    alignItems: "flex-end",
    justifyContent: 'space-between'
  },

});

export default styles;