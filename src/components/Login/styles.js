import { general, colors, fonts } from '../../styles';
import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  ...general,
 
  logo: {
    backgroundColor: colors.primary,      
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 60,
    flex: 4.5
  },

  forms: {
    flex: 4.5,
    paddingLeft: 22,
    paddingRight: 22,
    paddingTop: 34,
    alignItems: "stretch",
  },

  links: {
    flexDirection: 'row',    
    justifyContent: 'space-between'
  },

  linkBotao: {
    flex: 1,      
    height: 18      
  },

  linksTexto: {
    flex: 1,
    color: colors.primary,
    fontFamily: 'Montserrat-Regular'
  },

});

export default styles;