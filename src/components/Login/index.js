import React, { Component } from 'react';
import {
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Text
} from 'react-native';

import styles from './styles';
import { logo, circleLeft, circleRight } from '../../static/images_require';
import { Actions } from 'react-native-router-flux';

class Login extends Component {
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.logo} elevation={1}>
          <Image source={logo} />
        </View>

        <View style={styles.forms}>
          <TextInput style={styles.input}
            autoCapitalize={'none'}
            placeholder={'E-mail'}
          />
          <TextInput style={styles.input}
            secureTextEntry={true}
            autoCapitalize={'none'}
            placeholder={'Senha'}
          />

          <TouchableOpacity style={styles.botao}>
            <Text style={styles.botaoTexto}>Entrar</Text>
          </TouchableOpacity>

          <View style={styles.links}>
            <TouchableOpacity onPress={() => { Actions.esqueciSenha() }} style={styles.linkBotao}>
              <Text style={styles.linksTexto}>Esqueci minha senha</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { Actions.criarConta() }} style={[styles.linkBotao, { alignItems: 'flex-end' }]}>
              <Text style={styles.linksTexto}>Criar uma conta</Text>
            </TouchableOpacity>
          </View>


        </View>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end' }}>
          <Image source={circleLeft} />
          <Image source={circleRight} />
        </View>

      </View>
    )
  }
}

export default Login;
