import React, { Component } from 'react';
import { View, Image } from 'react-native';
import colors from '../../styles/colors';
import { logo } from '../../static/images_require';

class SplashScreen extends Component {
    render() {
        return (
            <View style={{flex: 1, backgroundColor: colors.primary, alignItems: 'center', justifyContent: 'center'}}>
                <Image source={logo} />
            </View>
        )
    }
}

export default SplashScreen;
