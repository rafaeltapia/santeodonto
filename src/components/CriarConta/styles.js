import { general, colors, fonts } from '../../styles';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    ...general,

    logo: {
        backgroundColor: colors.primary,
        //justifyContent: 'space-around',
        //alignItems: 'center',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 60,
        flex: 2
    },

    txtConta: {
        fontFamily: 'Montserrat-Bold',
        color: '#FFF',
        fontSize: 23,
    },

    txtCriarNova: {
        fontFamily: 'Montserrat-Regular',
        color: '#FFF',
        fontSize: 23,
    },

    forms: {
        flex: 3,
        justifyContent: 'flex-start',
        padding: 22,
        marginTop: 20
    },

    input: {
        fontFamily: 'Montserrat-Medium',
        borderRadius: 8,
        borderColor: '#ECECEC',
        color: colors.font,
        fontSize: fonts.input,
        borderWidth: 1,
        height: 57,
        textAlign: 'center',
        marginBottom: 17
    },

    botao: {
        borderRadius: 8,
        height: 57,
        backgroundColor: colors.primary,
        alignItems: 'center',
        justifyContent: 'center'
    },

    botaoTexto: {
        fontFamily: 'Montserrat-Bold',
        color: '#FFF',
        fontSize: fonts.input,
    },
    
    padding: {
        padding: 22
    },

    viewBotao: {
        flex: 2,
        marginTop: 30
    }

});

export default styles;