import React, { Component } from 'react';
import {
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  ImageBackground
} from 'react-native';

import styles from './styles';
import { typeAlias } from '@babel/types';
import { logo, circleLeft, circleRight } from '../../static/images_require';
import { TextInputMask } from 'react-native-masked-text';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

class CriarConta extends Component {

  constructor() {
    super()

    this.state = {
      proximoForm: false,
      email: '',
      senha: '',
      confirmarSenha: '',
      nome: '',
      telefone: '',
      clinica: ''
    }
  }

  _renderForm = () => {
    const { proximoForm } = this.state;
    if (!proximoForm) {
      return (
        <View>

          <View>

            <TextInput style={styles.input}
              keyboardType={'email-address'}
              autoCapitalize={'none'}
              placeholder={'E-mail'}
              onChangeText={email => this.setState({ email })}
            />
            <TextInput style={styles.input}
              secureTextEntry={true}
              autoCapitalize={'none'}
              placeholder={'Senha'}
              onChangeText={senha => this.setState({ senha })}
            />

            <TextInput style={styles.input}
              secureTextEntry={true}
              autoCapitalize={'none'}
              placeholder={'Confirme a Senha'}
              onChangeText={confirmarSenha => this.setState({ confirmarSenha })}
            />

          </View>

          <View style={styles.viewBotao}>

            <TouchableOpacity onPress={() => this.setState({ proximoForm: true })} style={styles.botao}>
              <Text style={styles.botaoTexto}>Próximo</Text>
            </TouchableOpacity>

          </View>

        </View>
      )
    } else {
      return (
        <View>

          <View>

            <TextInput style={styles.input}
              autoCapitalize={'none'}
              placeholder={'Seu Nome'}
              onChangeText={nome => this.setState({ nome })}
            />

            <TextInputMask style={styles.input}
              autoCapitalize={'none'}
              placeholder={'Telefone'}
              type={'cel-phone'}
              onChangeText={telefone => this.setState({ telefone })}
              maxLength={15}
              keyboardType={'number-pad'}
              value={this.state.telefone}
            />

            <TextInput style={styles.input}
              autoCapitalize={'none'}
              placeholder={'Nome da clínica'}
              onChangeText={clinica => this.setState({ clinica })}
            />

          </View>

          <View style={styles.viewBotao}>

            <TouchableOpacity style={styles.botao}>
              <Text style={styles.botaoTexto}>Criar Conta</Text>
            </TouchableOpacity>

          </View>

        </View>
      )
    }
  }

  backHandler = () => {
    const { proximoForm } = this.state;

    if (proximoForm) {
      this.setState({ proximoForm: false })
    } else {
      Actions.login()
    }
  }

  render() {

    return (
      <View style={styles.container}>

        <View style={styles.logo}>
          <TouchableOpacity style={{ alignItems: 'flex-start' }} onPress={this.backHandler}>
            <Icon style={{ fontSize: 42, color: '#fff' }} type='MaterialIcons' name='chevron-left' />
          </TouchableOpacity>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-around' }}>
            <Image style={{ width: 112, height: 38 }} source={logo} />
            <Text style={styles.txtCriarNova}>Criar nova <Text style={styles.txtConta}>conta</Text></Text>
          </View>
        </View>

        <View style={styles.forms}>
          {this._renderForm()}
        </View>

        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end' }}>
          <Image source={circleLeft} />
          <Image source={circleRight} />
        </View>

      </View>
    )
  }
}

export default CriarConta;
