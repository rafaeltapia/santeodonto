import React from 'react';
import { StatusBar } from 'react-native';
import { colors } from './styles';
import { Provider } from 'react-redux';
import store from '../src/static/store';
import Routes from '../src/static/Routes';

const AppRoot = () => {
    return (
        <Provider store={store}>
            <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
            <Routes />
        </Provider>
    )
}

export default AppRoot;