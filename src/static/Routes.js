import React from 'react';
import {
  SplashScreenContainer,
  LoginContainer,
  EsqueciSenha,
  CriarConta
} from '../containers';

import { Router, Scene, Stack } from 'react-native-router-flux';

const Routes = () => (
    <Router>
      <Stack key="root">
        <Scene hideNavBar key="splashScreen" component={SplashScreenContainer} />
        <Scene hideNavBar initial key="login" component={LoginContainer} />
        <Scene hideNavBar key="esqueciSenha" component={EsqueciSenha} />
        <Scene hideNavBar  key="criarConta" component={CriarConta} />
      </Stack>
    </Router>
  );

export default Routes;