export const logo = require('../assets/images/logo.png');
export const circleRight = require('../assets/images/circle_right.png');
export const circleLeft = require('../assets/images/circle_left.png');
export const success = require('../assets/images/success_icon.png');