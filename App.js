import React from 'react';
import AppRoot from './src';

const App = () => {
  return (
    <AppRoot />
  );
};

export default App;
